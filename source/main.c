#include <stdio.h>
#include "dice.h"

int main() //main function
{

    int numFaces, numDices;

    initializeSeed(); //this is to be sure that we have different results every new execution
    printf("How many sides do you wish your dice to have? ");
    scanf("%d", &numFaces);
    printf("How many dices do you wish to throw? ");
    scanf("%d", &numDices);
    for (int i=1; i<= numDices; i++)
    printf("\nLet's roll the dice #%d: %4d\n", i, rollDice(numFaces)); //this will print out our dice number
    return 0;
}
